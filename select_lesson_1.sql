SELECT
	*
FROM
	film, language
WHERE 
	film.language_id = language.language_id 
#декартово произведение
_______________

SELECT
	film.film_id, film.title, language.name
FROM
	film JOIN language
	ON film.language_id = language.language_id 
_______________

SELECT
	film.film_id, film.title, language.name
FROM
	film JOIN language # внутреннее соединение
	ON film.language_id = language.language_id 
WHERE
	film.title LIKE '%Agent%'
_______________

SELECT
	film.film_id, film.title, language.name
FROM
	film RIGHT JOIN language
	ON film.language_id = language.language_id 
_______________

SELECT
	film.film_id, film.title, language.name
FROM
	film RIGHT JOIN language # правое соединение без элементов левой таблицы
	ON film.language_id = language.language_id
WHERE
	film.film_id IS NULL
_______________
#соединили 4 таблицы
SELECT
	film.film_id, 
	film.title, 
	language.name,
	category.name
FROM
	film 
	JOIN language
	ON film.language_id = language.language_id
	JOIN film_category
	ON film.film_id = film_category.film_id
	JOIN category
	ON film_category.category_id = category.category_id
_______________

SELECT 
	customer.customer_id,
	customer.last_name,
	customer.first_name,
	COUNT(rental.rental_id),
	SUM(payment.amount)
FROM
	customer
	JOIN rental
	ON customer.customer_id = rental.customer_id
	JOIN payment
	ON customer.customer_id = payment.customer_id
GROUP BY
	customer.customer_id,
	customer.last_name,
	customer.first_name
HAVING
	AVG(payment.amount) > 5
_______________

SELECT 
	customer.customer_id,
	customer.last_name,
	customer.first_name,
	country.country,
	COUNT(rental.rental_id),
	SUM(payment.amount)
FROM
	customer
	JOIN rental
	ON customer.customer_id = rental.customer_id
	JOIN payment
	ON customer.customer_id = payment.customer_id
	JOIN address
	ON customer.address_id = address.address_id
	JOIN city
	ON address.city_id = city.city_id
	JOIN country
	ON city.country_id = country.country_id
	
	
GROUP BY
	customer.customer_id,
	customer.last_name,
	customer.first_name,
	country.country
HAVING
	AVG(payment.amount) > 5
__________________