CREATE TABLE user (
user_id NUMBER(5) PRIMARY KEY,
lastname VARCHAR2(50) NOT NULL,
firstname VARCHAR2(50) NOT NULL
)

CREATE TABLE post (
post_id NUMBER(5) PRIMARY KEY,
user_id NUMBER(5) NOT NULL,
group_id NUMBER(5) NOT NULL,
content VARCHAR2(100) NOT NULL
)

CREATE TABLE like (
user_id NUMBER(5) NOT NULL,
post_id NUMBER(5) NOT NULL
PRIMARY KEY(post_id, user_id)
)

CREATE TABLE comment (
comment_id NUMBER(5) PRIMARY KEY,
user_id NUMBER(5) NOT NULL,
post_id NUMBER(5) NOT NULL,
content VARCHAR2(100) NOT NULL
)
CREATE TABLE group (
group_id NUMBER(5) PRIMARY KEY,
name VARCHAR2(20) NOT NULL
)
CREATE TABLE user_group ( 
user_id NUMBER(5) NOT NULL,
group_id NUMBER(5) NOT NULL,
PRIMARY KEY (group_id, user_id)
)

SELECT group.name, user.lastname, user.firstname, MAX(sum) 
FROM (
		SELECT group.name, user.lastname, user.firstname, COUNT(comment_id) as sum 
		FROM 
			group JOIN user_group ON group.group_id = user_group.group_id
			JOIN user ON user.user_id = user_group.user_id
			JOIN post ON post.group_id = group.group_id
			JOIN comment ON comment.user_id = user.user_id
		GROUP BY
			group.name, user.lastname, user.firstname
	)
GROUP BY
	group.name, user.lastname, user.firstname

#######
SELECT user.lastname, user.firstname, AVG(num_likes) as avg_likes
FROM 
	user JOIN post ON user.user_id = post.user_id
	JOIN 
	(
	SELECT post.post_id as posts, COUNT(like.post_id) as num_likes
	FROM post JOIN like ON like.post_id = post.post_id
	GROUP BY 
		post.post_id
) ON posts = post.user_id

GROUP BY 
	 user.lastname, user.firstname
ORDER BY
	avg_likes DESC



