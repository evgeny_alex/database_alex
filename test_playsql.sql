1. Выбрать id актеров с фамилией Guiness
SELECT actor_id 
FROM actor 
WHERE last_name = 'Guiness'

2. Вывести первые 5 клиентов, которые брали больше 
сех фильмов (не считать тех, кто не брал фильмы 
	вообще). Вывести в порядке убывания количества 
ятых фильмов. Если клиент брал один фильм пять раз 
 примеру), то в данном случае к сумме по клиенту 
прибавлять 5, а не 1.

SELECT customer.customer_id as customer_id, COUNT(answer.film_id) as total_count 
FROM customer,
	(
		SELECT inventory.film_id as film_id, rental.customer_id as customer_id
		FROM 
			inventory 
			JOIN rental ON inventory.inventory_id = rental.inventory_id
	) as answer
WHERE customer.customer_id = answer.customer_id
GROUP BY customer.customer_id
HAVING COUNT(customer.customer_id) = 5
ORDER BY total_count

SELECT customer_id, COUNT(film_id) as total_count
FROM 
	rental JOIN inventory ON rental.inventory_id = inventory.inventory_id
GROUP BY customer_id
HAVING COUNT(film_id) > 40
ORDER BY total_count DESC

3. Посчитать, сколько было выдано фильмов по каждому
 из магазинов (складов, store). Соотносить 
выдачу фильма с магазином по привязке носителя к нему (inventory).

SELECT store.store_id as store_id, COUNT(inventory_id) as rental_cnt 
FROM 
	store JOIN staff ON store.store_id = staff.store_id
	JOIN rental ON staff.staff_id = rental.staff_id

GROUP BY store.store_id

SELECT store.store_id as store_id, COUNT(inventory_id) as rental_cnt 
FROM 
	store JOIN inventory ON store.store_id = inventory.store_id

GROUP BY store.store_id

4. Найти фильмы, оплаченные более одного раза, с актерами, которые
 снялись как минимум в 25 фильмах.

5. Для фильмов, выданных в прокат в течение месяца до 2005-07-07, определить 
количество различных клиентов (за все время), которые оплачивали фильмы. 
Вывести первые 8 строк в порядке убывания количества различных клиентов.

