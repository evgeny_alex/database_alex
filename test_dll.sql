1. Создать таблицы:

persons(id, lastname, firstname)
jobs(id, name)
employees(id, person_id, job_id)


CREATE TABLE persons (
id NUMBER(5),
lastname VARCHAR2(50) NOT NULL,
firstname VARCHAR2(50) 
)

CREATE TABLE jobs (
id NUMBER(5),
name VARCHAR2(50) NOT NULL
)

CREATE TABLE employees (
id NUMBER(5), 
person_id NUMBER(5),
job_id NUMBER(5)
)

2. Придумать для каждой таблицы новый столбец и добавить их с помощью ALTER TABLE

ALTER TABLE persons ADD experience NUMBER(5)
ALTER TABLE jobs ADD prestige VARCHAR2(50);
ALTER TABLE employees ADD salary NUMBER(10, 2);

3. Добавить первичные ключи в каждую таблицу и внешние ключи для таблицы employees

ALTER TABLE persons ADD PRIMARY KEY (id)
ALTER TABLE jobs ADD PRIMARY KEY (id)
ALTER TABLE employees ADD PRIMARY KEY (id)
ALTER TABLE employees ADD FOREIGN KEY (person_id) REFERENCES persons (id);
ALTER TABLE employees ADD FOREIGN KEY (job_id) REFERENCES jobs (id);

4. Добавить ограничение уникальности пары фамилия + имя

ALTER TABLE persons2 
ADD UNIQUE(lastname, firstname);

5. Добавить в каждую таблицу по 3 строки тестовых данных (id заполнять самостоятельно по порядку с 1 по 3).
#добавление в 1 таблицу
INSERT INTO persons VALUES(1, 'Smith', 'Jake', 5);
INSERT INTO persons VALUES(2, 'Black', 'John', 4);
INSERT INTO persons VALUES(3, 'White', 'Sara', 3);

#добавление в 2 таблицу
INSERT INTO jobs VALUES(1, 'director', 'high');
INSERT INTO jobs VALUES(2, 'officer', 'medium');
INSERT INTO jobs VALUES(3, 'doctor', 'medium');

#добавление в 3 таблицу
INSERT INTO employees VALUES(1, 1, 1, 10);
INSERT INTO employees VALUES(2, 2, 2, 10);
INSERT INTO employees VALUES(3, 3, 3, 10);

6. Изменить для человека (в таблице persons) с id = 1 значение столбца, добавленного на шаге 2 на любое другое.

UPDATE persons SET experience = 10 WHERE id=1

7. Добавить в таблицу employees поля salary и premium_size (для хранения величин зарплаты и премии). Установить их всем по умолчанию в 10000 и 2000 соответственно.

ALTER TABLE employees ADD premium_size NUMBER(10, 2) DEFAULT 100;

8. Для работника с id равным 2 увеличить зарплату на 10%, а премию на 30%

UPDATE employees SET salary = salary * 1.1  WHERE id=1
UPDATE employees SET premium_size = premium_size * 1.3  WHERE id=1

9. Удалить работника с id , равным 3 (из таблицы employees).

DELETE FROM employees WHERE id = 3

10. Удалить ограничение , добавленное на шаге 3.

ALTER TABLE employees DROP CONSTRAINT SYS_C0086783688;