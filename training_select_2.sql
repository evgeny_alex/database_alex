SELECT
	customer.last_name,
	customer.first_name,
	COALESCE(payment_customer.customer_amount, 0) AS customer_amount,
	COALESCE(total_amounts.other_amount, 0) AS other_amount,
	CASE WHEN total_amounts.other_amount > 0 AND payment_customer.customer_amount IS NOT NULL THEN 
		payment_customer.customer_amount / (total_amounts.other_amount)
		ELSE 0
	END AS amount_ratio
FROM
	customer LEFT JOIN	
		(
			SELECT
				customer_id,
				SUM(amount) AS customer_amount
			FROM 
				payment
			WHERE 
				payment_date BETWEEN '2007-03-01'::date AND '2007-03-07'::date
			GROUP BY
				customer_id
		) payment_customer ON payment_customer.customer_id = customer.customer_id,
		(
			SELECT
			SUM(amount) AS other_amount
			FROM 
				payment
			WHERE 
				payment_date BETWEEN '2007-03-01'::date AND '2007-03-07'::date
		) total_amounts