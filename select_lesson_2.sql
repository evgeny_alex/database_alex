SELECT
	last_name,
	first_name
FROM customer
____________________

SELECT
	last_name,
	first_name
FROM customer
UNION
SELECT
	last_name,
	first_name
FROM staff
UNION
SELECT
	last_name,
	first_name
FROM actor
___________________

SELECT
	email,
	last_name,
	first_name
FROM customer
UNION ALL # объединение только с одинаковыми полями, либо NULL
SELECT	
	email,
	last_name,
	first_name
FROM staff
UNION ALL
SELECT	
	#NULL
	last_name,
	first_name
FROM actor

___________________

SELECT * 
FROM (
	SELECT
		last_name,
		first_name
	FROM customer
	UNION ALL
	SELECT	
		last_name,
		first_name
	FROM staff
	UNION ALL
	SELECT	
		last_name,
		first_name
	FROM actor
) all_names

___________________

SELECT 
	all_names.last_name,
	COUNT(*) 
FROM (
	SELECT
		last_name,
		first_name
	FROM customer
	UNION ALL
	SELECT	
		last_name,
		first_name
	FROM staff
	UNION ALL
	SELECT	
		last_name,
		first_name
	FROM actor
) all_names

GROUP BY
	all_names.last_name
HAVING
	COUNT(*) > 3

_____________________

SELECT
	all_names.last_name,
	COUNT(*)
FROM
	actor all_names
GROUP BY
	all_names.last_name
HAVING
	COUNT(*) > 3

_____________________

SELECT 
	all_names.last_name,
	all_names.first_name
FROM
	actor all_names
JOIN 
	(SELECT
		all_names.last_name,
		COUNT(*)
	FROM
		actor all_names
	GROUP BY
		all_names.last_name
	HAVING
		COUNT(*) > 3
) double_names
ON all_names.last_name = double_names.last_name

_______________________
# клиентов распределяем по группам с помощью UNION ALL (1 вариант)
SELECT 
	group_sums.group_num,
	SUM(customer_sum)
FROM 
	(SELECT
		customer_id,
		SUM(amount) AS customer_sum,
		CASE 
			WHEN SUM(amount) < 100 THEN 'I group'
			WHEN SUM(amount) < 150 AND SUM(amount) >= 100 THEN 'II group'
			ELSE 'III group'
		END AS group_num
	FROM payment
	GROUP BY
		customer_id) group_sums
GROUP BY
	group_sums.group_num
ORDER BY
	group_sums.group_num

________________________

SELECT 
	customer_id,
	SUM (CASE
		WHEN amount <= 5 THEN 1
		ELSE 0
	END )AS less_than_5,
	SUM (CASE 
		WHEN amount > 5 THEN 1
		ELSE 0
	END) AS more_than_5,
	SUM (CASE 
		WHEN amount <= 5 THEN amount
		ELSE 0
	END) AS sum_less_than_5,
	SUM (CASE 
		WHEN amount > 5 THEN amount
		ELSE 0
	END) AS sum_more_than_5
FROM 
	payment
GROUP BY
	customer_id

_________________________

SELECT 
	customer_id,
	SUM (CASE 
		WHEN amount <= 5 THEN 1
		ELSE 0
	END )AS less_than_5,
	SUM (CASE 
		WHEN amount > 5 THEN 1
		ELSE 0
	END) AS more_than_5,
	SUM (CASE 
		WHEN amount <= 5 THEN amount
		ELSE 0
	END) AS sum_less_than_5,
	SUM (CASE 
		WHEN amount > 5 THEN amount
		ELSE 0
	END) AS sum_more_than_5
FROM 
	payment
WHERE customer_id IN
	(SELECT customer_id FROM customer WHERE first_name = 'John')
	
GROUP BY
	customer_id