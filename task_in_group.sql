Группа 1:
Александров Е.А., Бычков В. Е., Жосов Н. А.
Текст задачи:
Текстовое описание:
Таблица Группы:
-Факультет
-Номер
-Количество студентов
-Староста

Таблица Преподаватели:
-Код
-ФИО
-Предмет
-Должность
-Телефон
-Основное место работы

Таблица Предметы: 
-Код
-Название

Таблица Преподаватель-Предмет:
-Преподаватель 
-Предмет

Таблица Расписание:
-Код
-День недели
-Номер пары
-Группа
-Преподаватель
-Предмет
-Аудитория

Запросы:
1. Среднее количество студентов, посещающих пары конкретного преподавателя, в конкретный день недели.(100% посещаемость от групп)
2. Найти группы, у которых ведет один преподаватель, в один день и кол-во студентов в группе меньше 10.


Ответ от группы 2:
CREATE TABLE groups(
	department VARCHAR2(50) NOT NULL,
	id NUMBER(5) PRIMARY KEY,
	amount NUMBER(2) NOT NULL,
	monitor NUMBER(5) NOT NULL 
);

CREATE TABLE subjects(
	id NUMBER(4) PRIMARY KEY,
	name VARCHAR2(50) NOT NULL
);

CREATE TABLE teachers(
	id NUMBER(4) PRIMARY KEY,
	name VARCHAR2(120) NOT NULL,
	subject NUMBER(4) NOT NULL,
	phone_number NUMBER(11) NOT NULL,
	university VARCHAR2(50) NOT NULL
);

CREATE TABLE st (
	teacher_id NUMBER(4) NOT NULL REFERENCES teachers(id),
	subject_id NUMBER(4) NOT NULL REFERENCES subjects(id),
	PRIMARY KEY (teacher_id, subject_id)
);

CREATE TABLE timetable(
	id NUMBER(5) PRIMARY KEY,
	day VARCHAR2(50) NOT NULL,
	num NUMBER(1) NOT NULL,
	group_id NUMBER(5) NOT NULL REFERENCES groups(id),
	teacher_id NUMBER(4) NOT NULL REFERENCES teachers(id),
	subject_id NUMBER(4) NOT NULL REFERENCES subjects(id),
	cab NUMBER(4)
);

Запрос1:
SELECT 
day_amount.name, 
day_amount.day, 
AVG(day_amount.amount) 
FROM 
(SELECT 
timetable.day, 
teachers.name, 
groups.amount AS amount 
FROM 
timetable JOIN teachers 
ON 
timetable.teacher_id = teachers.id 
JOIN groups 
ON 
timetable.group_id = groups.id 
) day_amount 
GROUP BY 
day_amount.name, 
day_amount.day 
ORDER BY 
day_amount.name, day_amount.day


Запрос2:
SELECT
	DISTINCT group_id
FROM
	timetable
GROUP BY
	group_id,
	day,
	teacher_id
HAVING
	COUNT(DISTINCT teacher_id) = 1

Запрос1(ещё одна версия):


Оценка ответа группы 2:
…..

Оценка формулировки задачи от группы 3:
Ничего не понял, но очень интересно. Моё уважение за такие селекты 4Head
