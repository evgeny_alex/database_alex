1. Выбрать film_id фильма, у которого заголовок = Alien Center

SELECT
	film_id
FROM
	film
WHERE
	title = 'Alien Center'

2. Вывести адреса (address_id), на которые брали в прокат 
фильмы более 40 раз (без разницы - одни и те же или разные).

SELECT
	address_id
FROM
	customer JOIN rental ON customer.customer_id = rental.customer_id
HAVING
	COUNT(inventory_id) > 40
GROUP BY
	address_id

3. Среди категорий фильмов, представленных более чем 70 
картинами, отобразить фильмы, взятые в прокат более двух раз

#категории фильмов
SELECT
	category_id,
	COUNT(film_id)
FROM
	film_category
GROUP BY
	category_id
HAVING
	COUNT(film_id) > 70

# Фильмы взятые в прокат более 2 раз (готовый вариант)
SELECT
	title
FROM
	film JOIN inventory ON film.film_id = inventory.film_id
	JOIN rental ON inventory.inventory_id = rental.inventory_id
	JOIN film_category ON film.film_id = film_category.film_id,
	(
		SELECT
			category_id,
			COUNT(film_id) AS count
		FROM
			film_category
		GROUP BY
			category_id
		HAVING
			COUNT(film_id) > 70
	) AS category_cnt 
WHERE
	film_category.category_id = category_cnt.category_id
GROUP BY
	title
HAVING 
	COUNT(rental_id) > 2

4. Разделить все категории фильмов на 3 группы (назвать их “Group 1”,
 “Group 2”, “Group 3”),
 в зависимости от количества фильмов: [0-50], (50-60], (60 и более) .
 По каждой группе найти общую выручку со всех фильмов , выданных 
в прокат за месяц, предшествующий заданной в запросе дате - 2005-07-07

# Делим категории в группы
SELECT
	CASE 
		WHEN category_cnt.count <= 50 THEN 'Group 1'
		WHEN category_cnt.count < 60 AND category_cnt.count > 50 THEN 'Group 2'
		ELSE 'Group 3'
	END AS cat_group
FROM
	(
		SELECT
			category_id,
			COUNT(film_id) AS count
		FROM
			film_category
		GROUP BY
			category_id
	) AS category_cnt
GROUP BY
	cat_group
# Общая выручка по каждой категории
SELECT
	category_id,
	SUM(amount) AS sum 
FROM
	film_category JOIN film ON film_category.film_id = film.film_id
	JOIN inventory ON inventory.film_id = film.film_id
	JOIN rental ON rental.inventory_id = inventory.inventory_id
	JOIN payment ON payment.rental_id = rental.rental_id
WHERE
	rental.rental_date BETWEEN '2005-06-01'::date AND '2005-07-01'::date
GROUP BY
	category_id

#Cоединение
SELECT
	CASE 
		WHEN category_cnt.count <= 50 THEN 'Group 1'
		WHEN category_cnt.count < 60 AND category_cnt.count > 50 THEN 'Group 2'
		ELSE 'Group 3'
	END AS cat_group,
	SUM(sum_category.sum) AS total_sum
FROM
	(
		SELECT
			category_id,
			COUNT(film_id) AS count
		FROM
			film_category
		GROUP BY
			category_id
	) AS category_cnt,
	(
		SELECT
			category_id,
			SUM(amount) AS sum 
		FROM
			film_category JOIN film ON film_category.film_id = film.film_id
			JOIN inventory ON inventory.film_id = film.film_id
			JOIN rental ON rental.inventory_id = inventory.inventory_id
			JOIN payment ON payment.rental_id = rental.rental_id
		WHERE
			rental.rental_date BETWEEN '2005-06-07'::date AND '2005-07-07'::date
		GROUP BY
			category_id
	) AS sum_category 
WHERE
	category_cnt.category_id = sum_category.category_id
GROUP BY
	cat_group








