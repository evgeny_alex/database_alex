SELECT
	customer.last_name,
	customer.first_name,
	COALESCE(customer_amounts.customer_amount, 0) AS customer_amount,
	total_amounts.all AS other_amount,
	CASE WHEN total_amounts.all > 0 AND customer_amounts.customer_amount IS NOT NULL THEN 
		customer_amounts.customer_amount / (total_amounts.all + customer_amounts.customer_amount)
		ELSE 0
	END AS amount_ratio
FROM
	customer LEFT JOIN
		(SELECT
			customer_id,
			SUM(amount) AS customer_amount
		FROM 
			payment
		WHERE
			payment_date BETWEEN '2007-03-01'::date AND '2007-03-07'::date
		GROUP BY
		customer_id) customer_amounts ON customer.customer_id = customer_amounts.customer_id,
		(SELECT
			SUM(amount) AS all
		FROM 
			payment
		WHERE
			payment_date BETWEEN '2007-03-01'::date AND '2007-03-07'::date) total_amounts
		
	Вопросы:
- Что должен делать запрос (что планировал автор запроса) - Вывести сумму покупок каждого клиента, общую сумму покупок
и отношение суммы каждого клиента к общей сумме
- Что можно сказать про сумму customer_amount + other_amount Общая сумма всех покупок, всегда должна быть одинакова	
- Какие есть ошибки / замечания к запросу
- Какой правильный запрос?