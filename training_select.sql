#Условие: Среди фильмов, в которых снялся только один актер, найти те, которые были оплачены

SELECT film.title
FROM film, 
    (
        SELECT DISTINCT
        bought_films.film_id
        FROM 
            (
                SELECT film.film_id
                FROM 
                    film, film_actor 
                WHERE
                    film.film_id = film_actor.film_id
                GROUP BY 
                    film.film_id
                HAVING 
                    COUNT(film.film_id) = 1
            ) as single_actor_films, 
            (
                (inventory 
                JOIN rental ON inventory.inventory_id = rental.inventory_id) as aa 
                JOIN payment ON aa.rental_id = payment.rental_id) as bought_films
        WHERE (amount > 0) AND (single_actor_films.film_id = bought_films.film_id) 
    ) as answer
WHERE film.film_id = answer.film_id

##################
# DISTINCT - выбирает только разные элементы таблицы

SELECT DISTINCT actor_id 
FROM film_actor
ORDER BY actor_id DESC # группировка по убыванию

##################