 1. SELECT
	customer.last_name,
	customer.first_name,
	COALESCE(total_amounts.customer_amount, 0) AS customer_amount,
	COALESCE(total_amounts.other_amount, 0) AS other_amount,
	CASE WHEN total_amounts.other_amount > 0 THEN 
		total_amounts.customer_amount / (total_amounts.other_amount + total_amounts.customer_amount)
		ELSE 0
	END AS amount_ratio
FROM
	customer LEFT JOIN
	(SELECT
	payment_customer.customer_id,
	payment_customer.amount AS customer_amount,
	SUM(other_payments.amount) AS other_amount
	FROM (
		SELECT
				customer_id,
				SUM(amount) AS amount
			FROM 
				payment
			WHERE 
				payment_date BETWEEN '2007-03-01'::date AND '2007-03-07'::date
			GROUP BY
				customer_id) payment_customer
		JOIN (
			SELECT
				customer_id,
				SUM(amount) AS amount
			FROM 
				payment
			WHERE 
				payment_date BETWEEN '2007-03-01'::date AND '2007-03-07'::date
			GROUP BY
				customer_id) other_payments ON payment_customer.customer_id <> other_payments.customer_id
	GROUP BY
	payment_customer.customer_id,
	payment_customer.amount) total_amounts	
	ON customer.customer_id = total_amounts.customer_id

2.
SELECT
	film_id,
	SUM(replacement_cost) AS SUM
FROM 	
	film
GROUP BY 
	film_id

3. 
Вывести первые 5 клиентов, которые брали больше 
сех фильмов (не считать тех, кто не брал фильмы 
	вообще). Вывести в порядке убывания количества 
ятых фильмов. Если клиент брал один фильм пять раз 
 примеру), то в данном случае к сумме по клиенту 
прибавлять 5, а не 1.

SELECT customer_id, COUNT(film_id) as total_count
FROM 
	rental JOIN inventory ON rental.inventory_id = inventory.inventory_id
GROUP BY customer_id
LIMIT 5
ORDER BY total_count DESC

4. Посчитать, сколько было выдано фильмов по каждому
 из магазинов (складов, store). Соотносить 
выдачу фильма с магазином по привязке носителя к нему (inventory).
SELECT 
	store.store_id AS store,
	COUNT(inventory_id) AS count
FROM
	store JOIN staff ON staff.store_id = store.store_id
	JOIN payment ON payment.staff_id = staff.staff_id
	JOIN rental ON rental.rental_id = payment.rental_id
GROUP BY 
	store.store_id
5.  Найти фильмы, оплаченные более одного раза, с актерами, которые
 снялись как минимум в 25 фильмах.

 SELECT
 	film_inventory_count.film_id
 FROM
 	(
 		SELECT 
 			actor_id,
 			COUNT(film_id) as films_count
 		FROM
 			film_actor
 		GROUP BY
 			actor_id
 	)	actor_count JOIN film_actor ON film_actor.actor_id = actor_count.actor_id
 	JOIN (
 		SELECT
 			film_id,
 			COUNT(inventory_id) AS count
 		FROM
 			inventory
 		GROUP BY 
 			film_id
 	)	film_inventory_count ON film_actor.film_id = film_inventory_count.film_id
WHERE
	film_inventory_count.count > 1 AND
	actor_count.films_count > 24
GROUP BY
	film_inventory_count.film_id

6. Для фильмов, выданных в прокат в течение месяца до 2005-07-07, определить 
количество различных клиентов (за все время), которые оплачивали фильмы. 
Вывести первые 8 строк в порядке убывания количества различных клиентов.

SELECT 
	
